package listener;

import surface.PuzzleSurface;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class MyAnimationListener implements AnimationListener {

	public PuzzleSurface puzzleSurface;

	public MyAnimationListener(PuzzleSurface ps) {
		puzzleSurface = ps;
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// block on touch from accepting input during this time
		puzzleSurface.introFinished = false;
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// resume touch interaction
		puzzleSurface.introFinished = true;
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}
}