package sound;

import java.io.IOException;

import state.CommonVariables;
import state.CommonWork;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import data.Data;

/**
 * 
 * A class to extend Media Player and implement handling interfaces. I also
 * started implementing the ability to handle the sound changes due to incoming
 * notification sounds like phone or message alerts *
 * 
 * Should handle errors with headphones becoming unplugged and media player
 * states with opening and closing of application.
 * 
 * @author Rick
 * 
 */
public class MyMediaPlayer extends MediaPlayer implements
		MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
		AudioManager.OnAudioFocusChangeListener,
		MediaPlayer.OnCompletionListener {

	CommonVariables cv = CommonVariables.getInstance();
	CommonWork cw = CommonWork.getInstance();

	public MediaPlayer mediaPlayer;
	float actualVolume, maxVolume;
	Uri path = Uri.parse(Data.PATH + Data.TRACK_01);
	AudioManager am;
	boolean preparing, isSeeking;
	AsyncTask<Object, Object, MediaPlayer> asyncStart;
	int result;

	public void init() {
		am = (AudioManager) cv.context.getSystemService(Context.AUDIO_SERVICE);
		result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnCompletionListener(this);
	}

	public void start() {
		asyncStart = new AsyncTask<Object, Object, MediaPlayer>() {

			@Override
			protected MediaPlayer doInBackground(Object... arg0) {
				try {
					if (cv.playMusic) {
						if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
							if (!preparing) {
								preparing = true;
								mediaPlayer.reset();
								mediaPlayer
										.setAudioStreamType(AudioManager.STREAM_MUSIC);
								mediaPlayer.setDataSource(cv.context, path);
								mediaPlayer.setVolume(cv.volume, cv.volume);
								mediaPlayer.prepareAsync();
							}
						}
					}
				} catch (IllegalArgumentException | SecurityException
						| IllegalStateException | IOException e) {
					// try is for the setting of the data source
					e.printStackTrace();
				}
				return mediaPlayer;
			}
		};
		asyncStart.execute(new Object[] {});
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		// check for option to play music and resume last position
		if (preparing) {
			if (cv.playMusic) {
				if (cv.currentSoundPosition > 0) {
					isSeeking = true;
					mediaPlayer.seekTo(cv.currentSoundPosition);
				}
				if (!player.isPlaying())
					player.start();
			}
			preparing = false;
		}
	}

	@Override
	public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
		// stop and release the Media Player, if it's available
		if (preparing)
			preparing = false;
		start();
		return true;
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// Handle audio lowering and raising for other phone sounds
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			// resume play back
			if (mediaPlayer == null)
				init();
			else if (!mediaPlayer.isPlaying()) {
				start();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS:
			// lost focus for an unbounded amount of time. stop and release
			pause();
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			// lost focus for a short time, but we have to stop play back.
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			if (mediaPlayer != null) {
				setNewVolume(0.1f);
			}
			break;
		}
	}

	public void resume() {
		// media player should have been destroyed in last pause
		init();
		start();
	}

	public void pause() {
		if (am != null)
			am.abandonAudioFocus(this);
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying())
				mediaPlayer.pause();
			cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void destroy() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void setNewVolume(Float setVolume) {
		if (mediaPlayer.isPlaying())
			mediaPlayer.setVolume(setVolume, setVolume);
	}

	public void cleanUp() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void toggleMusic() {
		if (cv.playMusic) {
			cv.playMusic = false;
			cw.showToast(cv.context, "Music off");
			pause();
		} else {
			cv.playMusic = true;
			cw.showToast(cv.context, "Music on/restarted");
			resume();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		start();
	}
}